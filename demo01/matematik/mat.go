package matematik

type Parametre struct {
	X int
	Y int
}

func Topla(p1 Parametre) (int, int) {
	return p1.X + p1.Y, 4
}

func Cikar(p1 Parametre) int {
	return p1.X - p1.Y
}
