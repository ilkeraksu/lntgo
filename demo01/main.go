package main

import (
	"fmt"

	"gitlab.com/ilkeraksu/lntgo/demo01/matematik"
)

func main() {

	p1 := matematik.Parametre{X: 4, Y: 6}

	matematik.Yazdir()
	sonuc, _ := matematik.Topla(p1)
	fmt.Printf("Hello World! %v", sonuc)

}
